﻿USE paginaweb;

-- 1 Indicar el título del libro y nombre del autor que lo ha escrito

SELECT l.titulo, l.autor FROM libro l;  


-- 2 Indicar el nombre del autor y título del libro en el que han ayudado

SELECT a.libro, a1.nombre_completo, l.titulo FROM ayuda a JOIN autor a1 ON a.autor = a1.id_autor JOIN libro l ON a.libro = l.id_libro ; 

-- 3 Indicar los libros que se han descargado cada usuario con la fecga de descarga. Listar el login, fecha de descarga y id del libro

SELECT f.libro, f.usuario, f.fecha_descarga FROM fechadescarga f GROUP BY f.usuario ORDER BY f.libro;  

-- 4 Indicar los libros que se han descargado cada usuario con la fecha de descarga. Listar el login, correo. fecha de descarga y título del libro

SELECT d.libro, u.fecha_alta, u.login, u.email FROM usuario u JOIN descarga d ON u.login = d.usuario GROUP BY d.usuario ORDER BY d.libro;  

-- 5 Listar el número de libros que hay

SELECT COUNT(*) FROM libro l; 

-- 6 Listar el número de libros por colección (no hace falta colocar el nombre de la colección)

SELECT COUNT(*), l.coleccion FROM libro l GROUP BY l.coleccion; 

-- 7 Indicar la colección que tiene más libros (no hace falta colocar nombre de la colección

SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion;
SELECT MAX( c1.numerodelibros) maximo FROM(SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1;
SELECT c1.coleccion FROM (SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1 
JOIN (SELECT MAX( c1.numerodelibros) maximo FROM(SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1) c2
ON c1.numerodelibros= c2.maximo;   

-- 8 Indicar la colección que tiene menos libros (no hace falta colocar el nombre de la colección)
    -- SUBCONSULTA C1
SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion;
    -- SUBCONSULTA C2
SELECT MIN( c1.numerodelibros) minimo FROM (SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1;
    -- CONSULTA FINAL
 SELECT c1.coleccion FROM (SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1 
JOIN (SELECT MIN( c1.numerodelibros) minimo FROM (SELECT COUNT(*) numerodelibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1)c2
 ON c1.numerodelibros= c2.minimo; 

-- 9 Indicar el nombre de la colección que tiene más libros

SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion;
SELECT MAX( c1.numlibros) maximo FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1;
SELECT c1.coleccion FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1 
  JOIN (SELECT MAX( c1.numlibros) maximo FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1) c2 
  ON c1.numlibros= c2.maximo;
    -- sacar el nombre de la coleccion
SELECT c.nombre, c3.coleccion FROM coleccion c JOIN (SELECT c1.coleccion FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1 
  JOIN (SELECT MAX( c1.numlibros) maximo FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1) c2 
  ON c1.numlibros= c2.maximo) c3 ON c.id_coleccion= c3.coleccion;         

-- 10 Indicar el nombre de la colección que tiene menos libros
SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion;
SELECT MIN( c1.numlibros) minimo FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1; 
SELECT c1.coleccion FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1 
JOIN (SELECT MIN( c1.numlibros) minimo FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1) c2 
ON c1.numlibros= c2.minimo;
    -- sacar el nombre de la coleccion
SELECT c.nombre, c3.coleccion FROM coleccion c JOIN (SELECT c1.coleccion FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1 
JOIN (SELECT MIN( c1.numlibros) minimo FROM (SELECT COUNT(*) numlibros, l.coleccion FROM libro l GROUP BY l.coleccion) c1) c2 
ON c1.numlibros= c2.minimo) c3 ON c.id_coleccion= c3.coleccion;   

-- 11 Indicar el nombre del libro que se ha descargado más veces
  SELECT COUNT(*) numdescargas, f.libro FROM fechadescarga f GROUP BY f.libro;
  SELECT MAX( c1.numdescargas) maximasdescargas FROM (SELECT COUNT(*) numdescargas, f.libro FROM fechadescarga f GROUP BY f.libro) c1;  
 SELECT l.titulo FROM libro l JOIN (SELECT MAX( c1.numdescargas) maximasdescargas FROM
  (SELECT COUNT(*) numdescargas, f.libro FROM fechadescarga f GROUP BY f.libro) c1) c2 ON c2.maximasdescargas= l.id_libro;

-- 12 Indicar el nombre del usuario que ha descargado más libros
SELECT COUNT(*) ndescarga, f.usuario FROM fechadescarga f GROUP BY f.usuario;
SELECT MAX(c1.ndescarga) maximo FROM (SELECT COUNT(*) ndescarga, f.usuario FROM fechadescarga f GROUP BY f.usuario) c1;
SELECT c1.usuario, c1.ndescarga FROM (SELECT COUNT(*) ndescarga, f.usuario FROM fechadescarga f GROUP BY f.usuario) c1 
  JOIN (SELECT MAX(c1.ndescarga) maximo FROM (SELECT COUNT(*) ndescarga, f.usuario FROM fechadescarga f GROUP BY f.usuario) c1) c2 
  ON c1.ndescarga= c2.maximo;    

-- 13 Indicar el nombre de los usuarios que han descargados más libros que adam3
 SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario; 
SELECT COUNT(*) descadam3 FROM fechadescarga f GROUP BY f.usuario HAVING f.usuario='adam3';
     -- consulta final
SELECT c1.usuario FROM ( SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario) c1 
  JOIN (SELECT COUNT(*) descadam3 FROM fechadescarga f GROUP BY f.usuario HAVING f.usuario='adam3') c2
  ON c1.ndescargas= c2.descadam3 WHERE c1.ndescargas > c2.descadam3; 
   
-- 14 Indicar el mes que más libros se han descargado 
  SELECT MONTH( f.fecha_descarga) mes, f.libro FROM fechadescarga f;
  SELECT COUNT(*) ndescarga, f.fecha_descarga FROM fechadescarga f GROUP BY f.fecha_descarga ;
  SELECT MAX( c1.ndescarga) maximas FROM ( SELECT COUNT(*) ndescarga, f.fecha_descarga FROM fechadescarga f GROUP BY f.fecha_descarga ) c1;
      -- consulta final
  SELECT DISTINCT MONTH( c1.fecha_descarga) FROM (SELECT COUNT(*) ndescarga, f.fecha_descarga FROM fechadescarga f GROUP BY f.fecha_descarga) c1 
  JOIN (SELECT MAX( c1.ndescarga) maximas FROM (SELECT COUNT(*) ndescarga, f.fecha_descarga FROM fechadescarga f GROUP BY f.fecha_descarga ) c1)c2 
  ON c1.ndescarga= c2.maximas;      